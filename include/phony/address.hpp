#pragma once

#include <cstdint>
#include <phony/context.hpp>
#include <phony/export.hpp>
#include <string>

namespace phony::address {

    PHONY_EXPORT uint32_t zip_code(context& ctx);

    PHONY_EXPORT std::string city(context& ctx);

    PHONY_EXPORT std::string city_prefix(context& ctx);

    PHONY_EXPORT std::string city_suffix(context& ctx);

    PHONY_EXPORT uint32_t street_address(context& ctx);

    PHONY_EXPORT std::string street_name(context& ctx);

    PHONY_EXPORT std::string street_prefix(context& ctx);

    PHONY_EXPORT std::string street_suffix(context& ctx);

    PHONY_EXPORT std::string unit(context& ctx);

    PHONY_EXPORT std::string country(context& ctx);

    PHONY_EXPORT std::string country_code(context& ctx);

    PHONY_EXPORT std::string state(context& ctx);

    PHONY_EXPORT std::string state_code(context& ctx);

    PHONY_EXPORT double_t latitude(context& ctx);

    PHONY_EXPORT double_t longitude(context& ctx);

}  // namespace phony::address
