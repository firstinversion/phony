#pragma once

#include <phony/context.hpp>
#include <phony/export.hpp>

namespace phony::name {

    PHONY_EXPORT std::string first_name(context& ctx);

    PHONY_EXPORT std::string last_name(context& ctx);

    //    PHONY_EXPORT std::string find_name(context& ctx);

    PHONY_EXPORT std::string job_title(context& ctx);

    PHONY_EXPORT std::string prefix(context& ctx);

    PHONY_EXPORT std::string suffix(context& ctx);

    PHONY_EXPORT std::string title(context& ctx);

    //    PHONY_EXPORT std::string job_descriptor(context& ctx);
    //
    //    PHONY_EXPORT std::string job_area(context& ctx);
    //
    //    PHONY_EXPORT std::string job_type(context& ctx);

}  // namespace phony::name
