#pragma once

#include <phony/context.hpp>
#include <phony/export.hpp>

namespace phony::internet {

    PHONY_EXPORT std::string email(context& ctx);

    PHONY_EXPORT std::string domain(context& ctx);

}  // namespace phony::internet
