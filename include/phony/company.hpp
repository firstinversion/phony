#pragma once

#include <phony/context.hpp>
#include <phony/export.hpp>

namespace phony::company {

    PHONY_EXPORT const std::vector<std::string>& suffixes(context& ctx);

    PHONY_EXPORT std::string company_name(context& ctx);

    PHONY_EXPORT std::string company_suffix(context& ctx);

    PHONY_EXPORT std::string catch_phrase(context& ctx);

    PHONY_EXPORT std::string bs(context& ctx);

    PHONY_EXPORT std::string catch_phrase_adjective(context& ctx);

    PHONY_EXPORT std::string catch_phrase_descriptor(context& ctx);

    PHONY_EXPORT std::string catch_phrase_noun(context& ctx);

    PHONY_EXPORT std::string bs_adjective(context& ctx);

    PHONY_EXPORT std::string bs_buzz(context& ctx);

    PHONY_EXPORT std::string bs_noun(context& ctx);

}  // namespace phony::company
