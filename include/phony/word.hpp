#pragma once

#include <phony/context.hpp>
#include <phony/export.hpp>

namespace phony::word {

    PHONY_EXPORT std::string any(context& ctx);

    PHONY_EXPORT std::string adjective(context& ctx);

    PHONY_EXPORT std::string adverb(context& ctx);

    PHONY_EXPORT std::string noun(context& ctx);

    PHONY_EXPORT std::string verb(context& ctx);

}  // namespace phony::word
