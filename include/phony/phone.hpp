#pragma once

#include <phony/context.hpp>
#include <phony/export.hpp>
#include <string>

namespace phony::phone {

    PHONY_EXPORT std::string phone(context& ctx);

    PHONY_EXPORT uint64_t country_code(context& ctx);

    PHONY_EXPORT uint16_t area_code(context& ctx);

    PHONY_EXPORT uint16_t exchange(context& ctx);

    PHONY_EXPORT uint16_t local(context& ctx);

    PHONY_EXPORT uint16_t extension(context& ctx);

}  // namespace phony::phone
