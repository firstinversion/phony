#pragma once

#include <phony/export.hpp>
#include <random>

namespace phony {

    class PHONY_EXPORT context {
    public:
        context();

        [[nodiscard]] std::default_random_engine& engine() { return _engine; }

    private:
        std::default_random_engine _engine;
    };

}  // namespace phony
