#include <phony/internet.hpp>

#include "primitives.hpp"
#include <fmt/format.h>
#include <phony/name.hpp>

#include <phony/resources/internet/domains.txt.hpp>

namespace phony::internet {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-interfaces-global-init"
#pragma ide diagnostic ignored "cert-err58-cpp"
    const auto list_domains =
        phony::primitives::build_string_list(phony::resource::domains_txt, phony::resource::domains_txt_len);
#pragma clang diagnostic pop

    std::string email(context& ctx) {
        const auto fname  = phony::name::first_name(ctx);
        const auto lname  = phony::name::last_name(ctx);
        const auto domain = phony::internet::domain(ctx);
        return fmt::format("{}.{}@{}", fname, lname, domain);
    }

    std::string domain(context& ctx) { return phony::primitives::select_from_vector(ctx, list_domains); }

}  // namespace phony::internet
