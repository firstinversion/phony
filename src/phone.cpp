#include <phony/phone.hpp>

#include <cassert>
#include <fmt/format.h>

namespace phony::phone {

    std::string phone(context& ctx) {
        auto format_dist = std::uniform_int_distribution<uint8_t>(0, 3);
        auto format      = format_dist(ctx.engine());

        switch (format) {
        case 0: return fmt::format("{}-{}-{}-{}", country_code(ctx), area_code(ctx), exchange(ctx), local(ctx));
        case 1: return fmt::format("+{} ({}) {}-{}", country_code(ctx), area_code(ctx), exchange(ctx), local(ctx));
        case 2:
            return fmt::format(
                "{}-{}-{}-{}x{}", country_code(ctx), area_code(ctx), exchange(ctx), local(ctx), exchange(ctx));
        case 3:
            return fmt::format(
                "+{} ({}) {}-{}x{}", country_code(ctx), area_code(ctx), exchange(ctx), local(ctx), exchange(ctx));
        default: assert(false); return "";
        }
    }

    uint64_t country_code(context& ctx) {
        auto dist = std::uniform_int_distribution<uint16_t>(1, 135);
        return dist(ctx.engine());
    }

    uint16_t area_code(context& ctx) {
        auto dist = std::uniform_int_distribution<uint16_t>(100, 999);
        return dist(ctx.engine());
    }

    uint16_t exchange(context& ctx) {
        auto dist = std::uniform_int_distribution<uint16_t>(100, 999);
        return dist(ctx.engine());
    }

    uint16_t local(context& ctx) {
        auto dist = std::uniform_int_distribution<uint16_t>(1000, 9999);
        return dist(ctx.engine());
    }

    uint16_t extension(context& ctx) {
        auto dist = std::uniform_int_distribution<uint16_t>(1, 32000);
        return dist(ctx.engine());
    }

}  // namespace phony::phone
