#include "primitives.hpp"

namespace phony::primitives {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "bugprone-use-after-move"
    std::vector<std::string> build_string_list(const char* data, std::size_t size) {
        std::vector<std::string> output;
        std::string              word;

        for (size_t i = 0; i < size; ++i) {
            const char c = data[i];
            if (c == '\n') {
                output.push_back(std::move(word));
            } else if (c == '\r') {
            } else {
                word.push_back(c);
            }
        }

        return output;
    }
#pragma clang diagnostic pop

}
