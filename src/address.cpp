#include <phony/address.hpp>

#include "primitives.hpp"
#include <phony/word.hpp>
#include <string>

#include <phony/resources/address/city_prefix.txt.hpp>
#include <phony/resources/address/city_suffix.txt.hpp>
#include <phony/resources/address/countries.txt.hpp>
#include <phony/resources/address/country_codes.txt.hpp>
#include <phony/resources/address/state_codes.txt.hpp>
#include <phony/resources/address/states.txt.hpp>
#include <phony/resources/address/street_prefixes.txt.hpp>
#include <phony/resources/address/street_suffixes.txt.hpp>
#include <phony/resources/address/unit_prefixes.txt.hpp>

namespace phony::address {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-interfaces-global-init"
#pragma ide diagnostic ignored "cert-err58-cpp"
    const auto list_city_prefix =
        phony::primitives::build_string_list(phony::resource::city_prefix_txt, phony::resource::city_prefix_txt_len);
    const auto list_city_suffix =
        phony::primitives::build_string_list(phony::resource::city_suffix_txt, phony::resource::city_suffix_txt_len);
    const auto list_street_prefixes = phony::primitives::build_string_list(phony::resource::street_prefixes_txt,
                                                                           phony::resource::street_prefixes_txt_len);
    const auto list_street_suffixes = phony::primitives::build_string_list(phony::resource::street_suffixes_txt,
                                                                           phony::resource::street_suffixes_txt_len);
    const auto list_unit_prefixes   = phony::primitives::build_string_list(phony::resource::unit_prefixes_txt,
                                                                         phony::resource::unit_prefixes_txt_len);
    const auto list_countries =
        phony::primitives::build_string_list(phony::resource::countries_txt, phony::resource::countries_txt_len);
    const auto list_country_codes = phony::primitives::build_string_list(phony::resource::country_codes_txt,
                                                                         phony::resource::country_codes_txt_len);
    const auto list_states =
        phony::primitives::build_string_list(phony::resource::states_txt, phony::resource::states_txt_len);
    const auto list_state_codes =
        phony::primitives::build_string_list(phony::resource::state_codes_txt, phony::resource::state_codes_txt_len);
#pragma clang diagnostic pop

    uint32_t zip_code(context& ctx) {
        auto dist = std::uniform_int_distribution<uint32_t>(10000, 99999);
        return dist(ctx.engine());
    }

    std::string city(context& ctx) { return city_prefix(ctx) + city_suffix(ctx); }

    std::string city_prefix(context& ctx) { return phony::primitives::select_from_vector(ctx, list_city_prefix); }

    std::string city_suffix(context& ctx) { return phony::primitives::select_from_vector(ctx, list_city_suffix); }

    uint32_t street_address(context& ctx) {
        auto dist = std::uniform_int_distribution<uint32_t>(1, 999999);
        return dist(ctx.engine());
    }

    std::string street_name(context& ctx) { return phony::word::noun(ctx); }

    std::string street_prefix(context& ctx) { return phony::primitives::select_from_vector(ctx, list_street_prefixes); }

    std::string street_suffix(context& ctx) { return phony::primitives::select_from_vector(ctx, list_street_suffixes); }

    std::string unit(context& ctx) { return phony::primitives::select_from_vector(ctx, list_unit_prefixes); }

    std::string country(context& ctx) { return phony::primitives::select_from_vector(ctx, list_countries); }

    std::string country_code(context& ctx) { return phony::primitives::select_from_vector(ctx, list_country_codes); }

    std::string state(context& ctx) { return phony::primitives::select_from_vector(ctx, list_states); }

    std::string state_code(context& ctx) { return phony::primitives::select_from_vector(ctx, list_state_codes); }

    double_t latitude(context& ctx) {
        auto dist = std::uniform_real_distribution<double_t>(-90, 90);
        return dist(ctx.engine());
    }

    double_t longitude(context& ctx) {
        auto dist = std::uniform_real_distribution<double_t>(-180, 180);
        return dist(ctx.engine());
    }

}  // namespace phony::address
