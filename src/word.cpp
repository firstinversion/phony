#include <phony/word.hpp>

#include "primitives.hpp"
#include <cassert>
#include <random>

#include <phony/resources/words/adjectives/adjectives_full.txt.hpp>
#include <phony/resources/words/adverbs/adverbs_full.txt.hpp>
#include <phony/resources/words/nouns/nouns_full.txt.hpp>
#include <phony/resources/words/verbs/verbs_full.txt.hpp>

namespace phony::word {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-interfaces-global-init"
#pragma ide diagnostic ignored "cert-err58-cpp"
    const auto adjectives = phony::primitives::build_string_list(phony::resource::adjectives_full_txt,
                                                                 phony::resource::adjectives_full_txt_len);
    const auto adverbs =
        phony::primitives::build_string_list(phony::resource::adverbs_full_txt, phony::resource::adverbs_full_txt_len);
    const auto nouns =
        phony::primitives::build_string_list(phony::resource::nouns_full_txt, phony::resource::nouns_full_txt_len);
    const auto verbs =
        phony::primitives::build_string_list(phony::resource::verbs_full_txt, phony::resource::verbs_full_txt_len);
#pragma clang diagnostic pop

    std::string any(context& ctx) {
        auto dist = std::uniform_int_distribution<size_t>(0, 3);
        auto res  = dist(ctx.engine());
        switch (res) {
        case 0: return adjective(ctx);
        case 1: return adverb(ctx);
        case 2: return noun(ctx);
        case 3: return verb(ctx);
        default: assert(false); return "";
        }
    }

    std::string adjective(context& ctx) { return phony::primitives::select_from_vector(ctx, adjectives); }

    std::string adverb(context& ctx) { return phony::primitives::select_from_vector(ctx, adverbs); }

    std::string noun(context& ctx) { return phony::primitives::select_from_vector(ctx, nouns); }

    std::string verb(context& ctx) { return phony::primitives::select_from_vector(ctx, verbs); }

}  // namespace phony::word
