#include <phony/company.hpp>

#include "primitives.hpp"
#include <fmt/format.h>
#include <phony/word.hpp>
#include <random>

#include <phony/resources/company/suffixes.txt.hpp>

namespace phony::company {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-interfaces-global-init"
#pragma ide diagnostic ignored "cert-err58-cpp"
    const auto list_suffixes =
        phony::primitives::build_string_list(phony::resource::suffixes_txt, phony::resource::suffixes_txt_len);
#pragma clang diagnostic pop

    const std::vector<std::string>& suffixes(context& ctx) { return list_suffixes; }

    std::string company_name(context& ctx) { return phony::word::noun(ctx); }

    std::string company_suffix(context& ctx) { return phony::primitives::select_from_vector(ctx, list_suffixes); }

    std::string catch_phrase(context& ctx) {
        return fmt::format("{} {} {}", catch_phrase_adjective(ctx), catch_phrase_descriptor(ctx), catch_phrase_noun(ctx));
    }

    std::string bs(context& ctx) { return fmt::format("{} {} {}", bs_buzz(ctx), bs_adjective(ctx), bs_noun(ctx)); }

    std::string catch_phrase_adjective(context& ctx) { return phony::word::adjective(ctx); }

    std::string catch_phrase_descriptor(context& ctx) { return catch_phrase_adjective(ctx); };

    std::string catch_phrase_noun(context& ctx) { return phony::word::noun(ctx); }

    std::string bs_adjective(context& ctx) { return phony::word::adjective(ctx); }

    std::string bs_buzz(context& ctx) { return fmt::format("{} {}", bs_adjective(ctx), bs_noun(ctx)); }

    std::string bs_noun(context& ctx) { return phony::word::noun(ctx); }

}  // namespace phony::company
