#pragma once

#include <phony/context.hpp>
#include <random>
#include <vector>

namespace phony::primitives {

    template <typename Elem>
    const Elem& select_from_vector(context& ctx, const std::vector<Elem>& vector) {
        auto dist = std::uniform_int_distribution<size_t>(0, vector.size());
        return vector[dist(ctx.engine())];
    }

    std::vector<std::string> build_string_list(const char* data, std::size_t size);

}  // namespace phony::primitives
