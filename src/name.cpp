#include <phony/name.hpp>

#include "primitives.hpp"
#include <random>

#include <phony/resources/names/first_names_female.txt.hpp>
#include <phony/resources/names/first_names_male.txt.hpp>
#include <phony/resources/names/job_titles.txt.hpp>
#include <phony/resources/names/last_names.txt.hpp>
#include <phony/resources/names/name_prefix.txt.hpp>
#include <phony/resources/names/name_suffix.txt.hpp>

namespace phony::name {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-interfaces-global-init"
#pragma ide diagnostic ignored "cert-err58-cpp"
    const auto first_names_male   = phony::primitives::build_string_list(phony::resource::first_names_male_txt,
                                                                       phony::resource::first_names_male_txt_len);
    const auto first_names_female = phony::primitives::build_string_list(phony::resource::first_names_female_txt,
                                                                         phony::resource::first_names_female_txt_len);
    const auto job_titles =
        phony::primitives::build_string_list(phony::resource::job_titles_txt, phony::resource::job_titles_txt_len);
    const auto last_names =
        phony::primitives::build_string_list(phony::resource::last_names_txt, phony::resource::last_names_txt_len);
    const auto name_prefixs =
        phony::primitives::build_string_list(phony::resource::name_prefix_txt, phony::resource::name_prefix_txt_len);
    const auto name_suffix =
        phony::primitives::build_string_list(phony::resource::name_suffix_txt, phony::resource::name_suffix_txt_len);
#pragma clang diagnostic pop

    std::string first_name(context& ctx) {
        std::bernoulli_distribution sex(0.5);
        if (sex(ctx.engine())) {
            return phony::primitives::select_from_vector(ctx, first_names_male);
        } else {
            return phony::primitives::select_from_vector(ctx, first_names_female);
        }
    }

    std::string last_name(context& ctx) { return phony::primitives::select_from_vector(ctx, last_names); }

//    std::string find_name(context& ctx) { return std::string(); }

    std::string job_title(context& ctx) { return phony::primitives::select_from_vector(ctx, job_titles); }

    std::string prefix(context& ctx) { return phony::primitives::select_from_vector(ctx, name_prefixs); }

    std::string suffix(context& ctx) { return phony::primitives::select_from_vector(ctx, name_suffix); }

    std::string title(context& ctx) { return phony::primitives::select_from_vector(ctx, job_titles); }

//    std::string job_descriptor(context& ctx) { return std::string(); }
//
//    std::string job_area(context& ctx) { return std::string(); }
//
//    std::string job_type(context& ctx) { return std::string(); }

}  // namespace phony::name
