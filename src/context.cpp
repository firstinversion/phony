#include <phony/context.hpp>

namespace phony {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-msc32-c"
    context::context() {
        std::random_device r;
        _engine.seed(r());
    }
#pragma clang diagnostic pop

}  // namespace phony
