#include <catch2/catch.hpp>
#include <phony/address.hpp>
#include <phony/context.hpp>

TEST_CASE("street name", "[address]") {
    auto ctx  = phony::context{};
    auto name = phony::address::street_name(ctx);
    REQUIRE(!name.empty());
}
