#include <iostream>
#include <phony/context.hpp>
#include <phony/phone.hpp>

int main() {
    auto ctx = phony::context{};
    std::cout << phony::phone::phone(ctx);
    return 0;
}
