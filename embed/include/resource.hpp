#pragma once

#include <cstddef>
#include <string>

class resource {
public:
    resource(const char* start, const size_t len)
        : resource_data(start)
        , data_len(len) {}

    [[nodiscard]] const char* const& data() const { return resource_data; }
    [[nodiscard]] const size_t&      size() const { return data_len; }

    [[nodiscard]] const char* begin() const { return resource_data; }
    [[nodiscard]] const char* end() const { return resource_data + data_len; }

    std::string toString() { return std::string(data(), size()); }

private:
    const char*  resource_data;
    const size_t data_len;
};

#define LOAD_RESOURCE(RESOURCE)                                                                                        \
    ([]() {                                                                                                            \
        extern const char   _resource_##RESOURCE[];                                                                    \
        extern const size_t _resource_##RESOURCE##_len;                                                                \
        return Resource(_resource_##RESOURCE, _resource_##RESOURCE##_len);                                             \
    })()
