from conans import ConanFile, CMake, tools


class Phony(ConanFile):
    name = "phony"
    version = "0.1.0"
    license = "MIT"
    author = "Andrew Rademacher <andrewrademacher@icloud.com>"
    url = "https://bitbucket.org/firstinversion/phony"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "cli": [True, False]}
    default_options = {"shared": "False", "cli": "False"}
    generators = "cmake"
    exports_sources = "*", "!build/*", "!.idea/*", "!cmake-build*/*"
    requires = ("boost/1.71.0@conan/stable",
                "fmt/6.0.0@bincrafters/stable")
    build_requires = "Catch2/2.10.2@catchorg/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure(
            defs={"BUILD_STATIC": not self.options.shared, "BUILD_CLI": self.options.cli, "USE_CONAN": True})
        cmake.build()
        if tools.get_env("CONAN_RUN_TEST", False):
            cmake.test(output_on_failure=True)

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["phony"]
