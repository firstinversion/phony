#!/usr/bin/env python3

import pandas as pd
import re

regex_numbers = re.compile(r"[0-9]*\)")
regex_job = re.compile(r"Jobs")
df = pd.read_excel(io="./job_titles.xlsx", sheet_name="Sheet1")
with open("./job_titles.txt", "w") as out:
    for index, row in df.iterrows():
        title = str(row[0])
        title = regex_numbers.sub("", title)
        title = regex_job.sub("", title)
        title = title.strip()
        out.write(title + "\n")
