#!/usr/bin/env python3

import pandas as pd


def convert_names(input_file, input_sheet_name, output_file):
    df = pd.read_excel(io=input_file, sheet_name=input_sheet_name)
    with open(output_file, "w") as out:
        for index, row in df.iterrows():
            name = str(row[0]).lower().title()
            out.write(name + "\n")


convert_names("./first_names.xlsx", "male", "./first_names_male.txt")
convert_names("./first_names.xlsx", "female", "./first_names_female.txt")
convert_names("./last_names.xlsx", "Sheet1", "./last_names.txt")
