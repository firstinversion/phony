set(CLI_HEADERS
        )
set(CLI_SOURCES
        )

add_executable(phony_cli src/main.cpp ${CLI_HEADERS} ${CLI_SOURCES})
target_link_libraries(phony_cli PRIVATE phony)
