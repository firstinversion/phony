#include <array>
#include <boost/program_options.hpp>
#include <functional>
#include <iostream>

#include <phony/address.hpp>
#include <phony/company.hpp>
#include <phony/context.hpp>
#include <phony/internet.hpp>
#include <phony/name.hpp>
#include <phony/phone.hpp>
#include <phony/word.hpp>

namespace po = boost::program_options;

std::function<void(phony::context&)> find_generator(const std::string& generator_name);

int main(int argc, char** argv) {
    po::options_description desc("Options");
    // clang-format off
    desc.add_options()
        ("generator", po::value<std::string>(), "Generator to call. (e.g. phone.phone or name.first_name")
        ("help", "Display help message.");
    // clang-format on

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 1;
    }

    if (vm.count("generator")) {
        auto gen = find_generator(vm["generator"].as<std::string>());
        auto ctx = phony::context{};
        gen(ctx);
    } else {
        throw std::invalid_argument("--generator is a required argument.");
    }

    return 0;
}

#define PRINT_RESULT(gen_name, gen_expression)                                                                         \
    if (generator_name == #gen_name) return [](phony::context& ctx) { std::cout << (gen_expression) << '\n'; };

std::function<void(phony::context&)> find_generator(const std::string& generator_name) {
    PRINT_RESULT(address.zip_code, phony::address::zip_code(ctx))
    PRINT_RESULT(address.city, phony::address::city(ctx))
    PRINT_RESULT(address.city_prefix, phony::address::city_prefix(ctx))
    PRINT_RESULT(address.city_suffix, phony::address::city_suffix(ctx))
    PRINT_RESULT(address.street_address, phony::address::street_address(ctx))
    PRINT_RESULT(address.street_prefix, phony::address::street_prefix(ctx))
    PRINT_RESULT(address.street_suffix, phony::address::street_suffix(ctx))
    PRINT_RESULT(address.unit, phony::address::unit(ctx))
    PRINT_RESULT(address.country, phony::address::country(ctx))
    PRINT_RESULT(address.country_code, phony::address::country_code(ctx))
    PRINT_RESULT(address.state, phony::address::state(ctx))
    PRINT_RESULT(address.state_code, phony::address::state_code(ctx))
    PRINT_RESULT(address.latitude, phony::address::latitude(ctx))
    PRINT_RESULT(address.longitude, phony::address::longitude(ctx))
    PRINT_RESULT(company.company_name, phony::company::company_name(ctx))
    PRINT_RESULT(company.company_suffix, phony::company::company_suffix(ctx))
    PRINT_RESULT(company.catch_phrase, phony::company::catch_phrase(ctx))
    PRINT_RESULT(company.bs, phony::company::bs(ctx))
    PRINT_RESULT(company.catch_phrase_adjective, phony::company::catch_phrase_adjective(ctx))
    PRINT_RESULT(company.catch_phrase_descriptor, phony::company::catch_phrase_descriptor(ctx))
    PRINT_RESULT(company.catch_phrase_noun, phony::company::catch_phrase_noun(ctx))
    PRINT_RESULT(company.bs_adjective, phony::company::bs_adjective(ctx))
    PRINT_RESULT(company.bs_buzz, phony::company::bs_buzz(ctx))
    PRINT_RESULT(company.bs_noun, phony::company::bs_noun(ctx))
    PRINT_RESULT(internet.email, phony::internet::email(ctx))
    PRINT_RESULT(internet.domain, phony::internet::domain(ctx))
    PRINT_RESULT(name.first_name, phony::name::first_name(ctx))
    PRINT_RESULT(name.last_name, phony::name::last_name(ctx))
    PRINT_RESULT(name.job_title, phony::name::job_title(ctx))
    PRINT_RESULT(name.prefix, phony::name::prefix(ctx))
    PRINT_RESULT(name.suffix, phony::name::suffix(ctx))
    PRINT_RESULT(name.title, phony::name::title(ctx))
    PRINT_RESULT(phone.phone, phony::phone::phone(ctx))
    PRINT_RESULT(phone.country_code, phony::phone::country_code(ctx))
    PRINT_RESULT(phone.area_code, phony::phone::area_code(ctx))
    PRINT_RESULT(phone.exchange, phony::phone::exchange(ctx))
    PRINT_RESULT(phone.local, phony::phone::local(ctx))
    PRINT_RESULT(phone.extension, phony::phone::exchange(ctx))
    PRINT_RESULT(word.any, phony::word::any(ctx))
    PRINT_RESULT(word.adjective, phony::word::adjective(ctx))
    PRINT_RESULT(word.adverb, phony::word::adverb(ctx))
    PRINT_RESULT(word.noun, phony::word::noun(ctx))
    PRINT_RESULT(word.verb, phony::word::verb(ctx))
    throw std::invalid_argument("generator not found");
}

#undef PRINT_RESULT
